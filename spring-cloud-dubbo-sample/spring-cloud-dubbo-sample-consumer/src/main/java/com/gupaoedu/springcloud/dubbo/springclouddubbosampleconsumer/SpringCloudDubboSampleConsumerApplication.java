package com.gupaoedu.springcloud.dubbo.springclouddubbosampleconsumer;

import com.gupaoedu.springcloud.dubbo.ISayHello;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringCloudDubboSampleConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudDubboSampleConsumerApplication.class, args);
    }

    // 一定要通过 dubbo 的reference 去获取 bean
    @Reference
    ISayHello sayHello;

    @GetMapping("/say")
    public String say() {
        System.out.println("say");
        return sayHello.sayHello("zjp");
    }

}
