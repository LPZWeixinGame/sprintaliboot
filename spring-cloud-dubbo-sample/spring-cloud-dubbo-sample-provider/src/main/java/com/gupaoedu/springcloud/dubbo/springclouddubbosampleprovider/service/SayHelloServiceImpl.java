package com.gupaoedu.springcloud.dubbo.springclouddubbosampleprovider.service;

import com.gupaoedu.springcloud.dubbo.ISayHello;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author zhangjianping
 */
// dubbo 的发布服务
@Service
public class SayHelloServiceImpl implements ISayHello {
    @Override
    public String sayHello(String msg) {
        return "Hello world! msg:" + msg;
    }
}
