package com.gupaoedu.springcloud.dubbo;

/**
 * @author zhangjianping
 */
public interface ISayHello {
    /**
     * 简单接口
     * @param msg 输入的msg
     * @return hello world
     */
    String sayHello(String msg);
}
