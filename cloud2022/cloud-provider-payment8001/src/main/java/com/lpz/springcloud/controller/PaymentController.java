package com.lpz.springcloud.controller;

import com.lpz.springcloud.entities.CommonResult;
import com.lpz.springcloud.entities.Payment;
import com.lpz.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    // http://localhost:8001/payment/create?t_serial=10&id=20
    // 这样测试
    @PostMapping(value = "/payment/create")
    public CommonResult creat(Payment payment) {
        log.info("**** 插入" + payment);
        int result = paymentService.create(payment);
        log.info("**** 插入结果" + result);

        if (result > 0) {
            return new CommonResult(200, "插入数据库成功", result);
        }
        else {
            return new CommonResult(444, "插入数据库失败", null);
        }
    }

    // http://localhost:8001/payment/get/2
    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("**** 获取值 = " + payment + ", byid = " + id);
        if (payment != null) {
            return new CommonResult(200, "获取成功", payment);
        }
        else {
            return new CommonResult(444, "没有找到，查询id="+id, null);
        }
    }
}
