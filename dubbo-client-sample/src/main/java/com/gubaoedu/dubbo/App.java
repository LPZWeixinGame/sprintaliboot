package com.gubaoedu.dubbo;

import com.gupaoedu.dubbo.server.ILoginService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        ILoginService loginService = null;

        // 这里的路径很重要，不要做：classpath:META-INF/spring/application.xml
        // 文件夹：META-INF；文件夹：spring；
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/application.xml");
        loginService = context.getBean(ILoginService.class);
        System.out.println(loginService.login("admin", "admin"));
    }
}
