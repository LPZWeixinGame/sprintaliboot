package com.gupaoedu.dubbo.server;

/**
 * @author zhangjianping
 * 发布要引用的接口api包，通过Maven install 打包
 */
public interface ILoginService {
    String login(String username, String password);
}
