package com.gupaoedu.dubbo.server;

import org.apache.dubbo.container.Main;

public class App {
    public static void main(String[] args) {
        // 使用 dubbo 的main 函数
        Main.main(args);
    }
}
